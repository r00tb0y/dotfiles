#trutruee If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/r00tb0y/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="rkj-repos"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# # Uncomment the following line to disable auto-setting terminal title.
# # DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="false"

# uncomment the following line if you want to disable marking untracked files
# under vcs as dirty. this makes repository status check for large repositories
# much, much faster.
# disable_untracked_files_dirty="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi


# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

# source nvm path convenience sript
source /usr/share/nvm/init-nvm.sh

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias sudo="sudo "
alias sd="sudo "
alias zfresh="source ~/.zshrc"
alias vfresh="source ~/.config/nvim/init.vim"
alias gclone="git clone "
alias gadd="git add . "
alias gpush="git push "
alias gstat="git status "
alias commit="git commit -m "
alias pushmaster="git push origin master "
alias v="nvim "
alias sv="sudo nvim "
alias ll="ls -ahl "
alias myip="curl http://ipecho.net/plain; echo"
alias grepf="find . | grep "
alias edzsh="v ~/.zshrc"
alias edvim="v ~/.config/nvim/init.vim"
alias sys-stat="sudo systemctl status "
alias sys-stop="sudo systemctl stop "
alias sys-start="sudo systemctl start "
alias sys-re="sudo systemctl restart "
alias sys-enable="sudo systemctl enable "
alias sys-kill="sudo systemctl disable "
alias yfp="yay -Ss "
alias yip="yay -S "
alias ydp="yay -Rns "
alias yup="sudo pacman -Syu "
alias rmf="rm -rf"
alias sfrm="sudo rm -rf"
alias tubedl="youtube-dl --add-metadata -i -o '%(title)s.%(ext)s'"
alias YT="mpsyt "
alias newsload="newsboat -r "
alias tgram="nctelegram "
alias wikisearch="s -p wikipedia -b lynx"
alias firewiki="s -p wikipedia "
alias wiki="wikicurses"
alias w3dic="s -p dict -b lynx "
alias overflow="s -p statckoverflow -b lynx "
alias tubefind="s -p youtube -b lynx "
alias archwiki="s -p archwiki -b lynx "
alias ddg="s -p duckduckgo -b lynx "
alias goodreads="s -p goodreads "
alias dicc="sdcv -c "


rootgf () {
  dir=`pwd`
  cd
  ...
  sudo find ! -path './run' | grep $1
  cd $dir
}

gccd () {
  if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: gccd <url>"
    echo "Clones a repo from url with git and cd's into its root folder immediately"
    return 1
  else
    git clone "$1" && cd $(basename $_ .git)
  fi
}

mkcd () {
  if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: mkcd <path><foldername>"
    echo "Create a folder and cd into it directly"
    return 1
  else
    mkdir "$1" && cd $_
  fi
}

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

# Search the web from terminal auto-completion
if [ -f $GOPATH/src/github.com/zquestz/s/autocomplete/s-completion.bash ]; then
    . $GOPATH/src/github.com/zquestz/s/autocomplete/s-completion.bash
fi
