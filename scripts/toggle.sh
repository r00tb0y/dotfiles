#!/usr/bin/env bash
# A custom version of the original toggle script
# v0.4 from the ArchLabs distro

NAME=$(basename "$0")
VER="0.2"

usage()
{
    cat <<- EOF

 USAGE:  $NAME [OPTIONS [ADDITIONAL]]

 OPTIONS:

     -h,--help         Display this message

     -v,--version      Display script version

     -p,--polybar      Toggle the configured polybar session, NO additional options

     -r,--redshift     Toggle redshift or daemon monitoring icon, can use toggle option

 ADDITIONAL:

     -t,--toggle       Toggle the program off/on, without this flag a monitor process will be started

EOF
}

toggle_polybar()
{
    if [[ $(pidof polybar) ]]; then
        pkill polybar
    else
        al-polybar-session
    fi
}

toggle_redshift()
{
    if (( opt == 1 )); then
        if [[ -f ~/.dim ]]; then
            xcalib -clear 
            rm ~/.dim
        else
          xrandr --output eDP1 --gamma 1.26:0.7:0.27
          xcalib -co 75 -a
          touch ~/.dim
        fi
        exit 0
    fi
    icon=""
    while true; do
        if [[ -f ~/.dim ]]; then
            echo "%{F#31aeff} $icon "     # Blue
        else
            echo "%{F#FF4136} $icon "     # Yellow
        fi
        sleep 3
    done
}

# Catch command line options
case $1 in
    -h|--help) usage ;;
    -v|--version) echo -e "$NAME -- Version $VER" ;;
    -p|--polybar) toggle_polybar ;;
    -r|--redshift)
        [[ $2 =~ (-t|--toggle) ]] && opt=1
        toggle_redshift
        ;;
    *) echo -e "Option does not exist: $1" && usage && exit 1
esac

