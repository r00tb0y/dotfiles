#!/bin/bash
#Checking fo VPN connections and dunst notify if lost

msgId="991049"
tun="$(ip link | awk '/tun0/ {print $2}')"

while true; do
  if [[ $tun != t* ]]; then
    dunstify -a "OpenVpn" -u critical -h "Watch Out!" -r "$msgId" "check network interfaces"
  else
    sleep 10
  fi
done
