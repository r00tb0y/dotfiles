#!/bin/bash

# Has a daily rate limit
# curl --silent ipinfo.io | jq --raw-output .ip

while [[ true ]];do
  if [[ $(cat /sys/class/net/wlp3s0/carrier) == 1 ||  $(cat /sys/class/net/enp0s25/carrier) == 1 ]]; then
    netinfo=$(curl --silent ifconfig.co/json)
    ip=$(echo "$netinfo" | jq --raw-output .ip)
    loc=$(echo "$netinfo" | jq --raw-output .country_iso)
    city=$(echo "$netinfo" | jq --raw-output .city)
    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
      if [[ -e /sys/class/net/tun0 && -e /sys/class/net/tun0/carrier ]];then
        echo "%{F#5fd700} $ip VPN@$city.$loc "
        break
      else
        echo "%{B#d70000} $ip ISP@$city.$loc "
        break
      fi
    else
      echo "%{B#d70000} $loc: IPv6@ISP "
    fi
    sleep 3s
  else
    echo "offline "
    sleep 10s
  fi
done
